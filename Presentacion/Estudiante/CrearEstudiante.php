<?php
//Incluir encabezado
include 'presentacion/encabezado.php';

//incluir condicional para crear el estudiante
?>
<div class="text-center mt-2">
    <h3>Registro de nuevos estudiantes</h3>
</div>

<div class="container">
    <?php if (isset($_POST["crear"])) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            Estudiante creado correctamente
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

    <?php } ?>
    <!-- Formulario para registro de estudiantes-->
    <form method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-6 mb-3">
                <label for="nameStudent" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="nombre" >
                <div id="nameHelp" class="form-text">Ingresa nombre del estudiante</div>
            </div>
            <div class="col-6 mb-3">
                <label for="nameStudent" class="form-label">Apellidos</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <div id="nameHelp" class="form-text">Ingresa apellido del estudiante</div>
            </div>
            <div class="col-6 mb-3">
                <label for="exampleInputPassword1" class="form-label">Codigo</label>
                <input type="text" class="form-control" id="exampleInputPassword1">
                <div id="nameHelp" class="form-text">Numero sin comas, puntos, ni letras</div>
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>

        </div>
        <div class="row align-items-center">
            <div class="col-5"></div>
            <button type="submit" class="col-2 btn btn-primary " name="crear">Crear</button>
            <div class="col-5"></div>
        </div>
    </form>
</div>