<!-- Container -->
<div>
        <nav class="navbar navbar-expand-lg bg-light">
                <div class="container-fluid">
                        <!--<a href="index.php"> <img src="img/logo.png" width="150px"></a>-->
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                        <li class="nav-item">
                                                <a class="nav-link active" aria-current="page" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php") ?>">Inicio</a>
                                        </li>
                                        <!-- Estudiante -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Estudiante
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Estudiante/CrearEstudiante.php") ?>">
                                                                        Crear Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Consultar estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Modificar datos estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Eliminar estudiante</a></li>
                                                </ul>
                                        </li>

                                        <!-- Docente -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Docente
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Crear Docente</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Modificar docente</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Consultar docente</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Eliminar docente</a></li>
                                                </ul>
                                        </li>

                                        <!-- Proyecto Curricular -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Proyecto Curricular
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Crear Proyecto curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Modificar proyecto curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Consultar proyecto curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Eliminar proyecto curricular</a></li>
                                                </ul>
                                        </li>

                                        <!-- Equipo -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Equipo
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Crear Equipo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Consultar equipo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Modificar equipo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Eliminar equipo</a></li>
                                                        
                                                        <!-- Revisar submodulos 
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>-->
                                                </ul>
                                        </li>

                                        <!-- Grupo -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Grupo
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Crear grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Consultar grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Modificar grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Eliminar grupo</a></li>
                                                        
                                                        <!-- Revisar submodulos 
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>-->
                                                </ul>
                                        </li>

                                        <!-- Paz y salvo -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Paz y salvo
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Asignatura -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Asignatura
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Facultad -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Facultad
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Espacio -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Espacio
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Horario -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Horario
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Mantenimiento -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Mantenimiento
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                        Estudiante</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Asignatura</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Proyecto Curricular</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Facultad</a></li>
                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/buscarProducto.php") ?>">
                                                                        Espacio</a></li>
                                                </ul>
                                        </li>

                                        <!-- Prestamo -->
                                        <li class="nav-item dropdown">
                                                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Prestamo
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                        <li class="nav-item dropdown"><a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        Prestamo
                                                                </a>
                                                                <!-- un menu dropbarr dentro de otro no funciona, revisar que otra forma podemos graficarlo para que se vea presentable y funcional -->
                                                                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php") ?>">
                                                                                        Estudiante</a></li>
                                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                                        Grupo</a></li>

                                                                </ul>
                                                        </li>

                                                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">
                                                                        Grupo</a></li>

                                                </ul>
                                        </li>

                                </ul>
                        </div>

                        <!-- Direccionamientos accesos -->
                        <div class="col-xs-12 col-lg-2 text-center">
                                <a href="http://tecelectrica.udistrital.edu.co:8080/tec-electricidad"> <img src="img/Logo U_D.png" width="100px"> </a>
                        </div>
                </div>
        </nav>
</div>