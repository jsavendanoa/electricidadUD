<?php
//PID guarda para el direccionamiento de las pag
$pid = "";
//Ejecuta el direccionamiento
if (isset($_GET["pid"])) {
  $pid = base64_decode($_GET["pid"]);
}

//En caso de implementar un login, estas no lo requieren
$paginasSinSesion = array(
  "presentacion/estudiante/crearestudiante.php",
  "presentacion/inicio.php"
);

//Esta es la segunda prueba del 23/03/2023
?>

<!doctype html>
<html lang="en">

<head>
  <!-- Link Bootstrap -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <!-- Script Bootstrap -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

</head>

<?php
if ($pid != "") {
  if (in_array($pid, $paginasSinSesion)) {
    include $pid;
  } else {
    include $pid;
    if (isset($_SESSION["id"]) && $_SESSION["id"] != "") {
      include $pid;
    } else {
      //no se activa inicio, ya que se repetiria encabezado y de mas...
      //include 'presentacion/inicio.php';
    }
  }
} else {
  include 'presentacion/encabezado.php';
}
?>

</html>